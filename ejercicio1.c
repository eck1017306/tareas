//
//  ejercicio1.c
//
//  Created by Erick Molina Vázquez on 21/08/15.
//  Copyright (c) 2015 Erick Molina Vázquez. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    char nombre[50];
    char propietario[50];
    int eslora;
    int manga;
    int maxtripulantes;
    int conteo;
}barco;

typedef struct
{
    char nombre[30];
    char apellidoP[30];
    char apellidoM[30];
    int edad;
    char rol[50];
    barco br;
}tripulante;

int poblacionBarco = 1;
int poblacionTripulantes = 1;
tripulante* addtrip(tripulante* trip, barco* emb);
barco* addbarco(barco* emb);
void printtrip(tripulante* trip);
void printbarco(barco* emb, tripulante* trip);

int main(int argc, char** argv) {
    
    int op=0;
    barco *emb = (barco *)malloc(sizeof(barco)); //variable donde se guardaran todos los barcos
    printf("Para empezar primero se agregará un barco al puerto.\nEsto se debe a que se necesitan tener definidos los barcos para agregar a los tripulantes.\n");
    emb = addbarco(emb);
    tripulante *trip = (tripulante *)malloc(sizeof(tripulante)); // variable donde se guardaran los tripulantes
    printf("Ahora se agregará un tripulante a ese barco.\n");
    trip=addtrip(trip,emb);
    char * menu = (char *)malloc(sizeof(char)*300);
    strcpy(menu,"\n::::\tMenu Principal\t::::\n1. Nueva Embarcación\n2. Nuevo tripulante\n3. Visualizar Embarcaciones con tripulantes\n4.Visualizar tripulantes\n Salir\n\nFavor de ingresar una opción.\n");
    while(op!=5)
    {
        printf("%s",menu);
        scanf("%d", &op);
        switch(op)
        {
            case 1:
                emb=addbarco(emb);
                break;
            case 2:
                trip=addtrip(trip, emb);
                break;
            case 3:
                printbarco(emb, trip);
                break;
            case 4:
                printtrip(trip);
                break;
            case 5:
                printf("Gracias por utilizar el programa");
                break;
            default:
                printf("Opcion no valida, favor de ingresar otra opcion");
                break;
        }
    }
    free(emb);
    free(trip);
    return (EXIT_SUCCESS);
}

tripulante* addtrip(tripulante *trip, barco *emb)
{
    int i, false=0, cancel=0;
    tripulante * temp;
    temp = trip;
    temp = (tripulante *)realloc(trip, sizeof(tripulante)*poblacionTripulantes);
    if(temp==NULL)
    {
        printf("Lo sentimos, ya no hay memoria para agregar participantes.");
        return trip;
    }
    trip=temp;
    free(temp);
    printf("Ingrese el nombre del tripulante (solo su nombre). ");
    scanf("%s",(trip)->nombre);
    printf("Ingrese el apellido paterno del tripulante. ");
    scanf("%s",(trip)->apellidoP);
    printf("Ingrese el apellido materno del tripulante. ");
    scanf("%s",(trip)->apellidoM);
    printf("Ingrese la edad del tripulante. ");
    scanf("%d",&((trip)->edad));
    printf("Ingrese el rol del tripulante. ");
    scanf("%s",(trip)->rol);
    while(false==0||cancel==0)
    {
        printf("Ingrese barco al que pertenece este tripulante.");
        scanf("%s",(trip)->br.nombre);
        for(i=0;i<poblacionBarco;++i)
        {
            if((emb)->nombre==(trip)->br.nombre)
            {
                if((emb)->maxtripulantes!=(emb)->conteo)
                {
                    ++(emb)->conteo;
                    false==1;
                }
                else
                {
                    printf("Este barco ya no cuenta con lugares");
                    printf("Si desea eliminar este tripulante para agregar primero el barco, oprima 0");
                    scanf("%d",&cancel);
                    cancel==1;
                }
            }
            if(i==(poblacionBarco-1))
            {
                printf("El barco no se encontro, se cancelará este tripulante ");
                cancel==1;
                break;
            }
        }
    }
    if(cancel==1)
    {
        trip = (tripulante *)realloc(trip, sizeof(tripulante)*(poblacionTripulantes-1));
        return trip;
    }
    ++poblacionTripulantes;
    return trip;
}

barco* addbarco(barco *emb)
{
    barco * temp;
    temp = emb;
    temp = (barco *)realloc(emb, sizeof(barco)*poblacionBarco);
    if(temp==NULL)
    {
        printf("Lo sentimos, ya no hay memoria para agregar participantes.");
        return emb;
    }
    emb=temp;
    free(temp);
    printf("Ingrese el nombre del barco. ");
    scanf("%s",(emb)->nombre);
    printf("Ingrese el nombre del propietario. ");
    scanf("%s",(emb)->propietario);
    printf("Ingrese la eslora del barco. ");
    scanf("%d",&((emb)->eslora));
    printf("Ingrese la manga del barco. ");
    scanf("%d",&((emb)->manga));
    printf("Ingrese el número máximo de tripulantes para este barco. ");
    scanf("%d",&((emb)->maxtripulantes));
    (emb+poblacionBarco)->conteo=0;
    ++poblacionBarco;
    printf("El nombre del barco es: %s\n",(emb)->nombre);
    printf("El propietario del barco es: %s\n",(emb)->propietario);
    printf("El eslora del barco es: %d\n",(emb)->eslora);
    printf("El manga del barco es: %d\n",(emb)->manga);
    return emb;
}

void printtrip(tripulante *trip)
{
    int i;
    for(i=0;i<poblacionTripulantes;++i)
    {
        printf("El tripulante se llama: %s %s %s\n",(trip+i)->nombre,(trip+i)->apellidoP,(trip+i)->apellidoM);
        printf("La edad del tripulante es: %d\n",(trip+i)->edad);
        printf("El rol del tripulante es: %s\n",(trip+i)->rol);
        printf("El barco al que pertence es: %s\n\n",(trip+i)->br.nombre);
    }
    return;
}

void printbarco(barco * emb, tripulante *trip)
{
    int i, j;
    for(i=0;i<poblacionBarco;++i)
    {
        printf("El nombre del barco es: %s\n",(emb+i)->nombre);
        printf("El propietario del barco es: %s\n",(emb+i)->propietario);
        printf("La slora del barco es: %d\n",(emb+i)->eslora);
        printf("La manga del barco es: %d\n",(emb+i)->manga);
        printf("El máximo número de tripulantes del barco es: %d\n",(emb+i)->maxtripulantes);
        printf("El barco lleva %d puestos asignados, los tripulantes son:\n",(emb+i)->conteo);
        for(j=0;j<poblacionTripulantes;++j)
        {
            if((trip+j)->br.nombre==(emb+i)->nombre)
                printf("%d.- %s %s %s",j,(trip+j)->nombre,(trip+j)->apellidoP,(trip+j)->apellidoM);
        }
    }
    return;
}

